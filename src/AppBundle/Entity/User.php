<?php // src/AppBundle/Entity/User.php
 
namespace AppBundleEntity;
 
use FOSUserBundleModelUser as BaseUser;
use DoctrineORMMapping as ORM;
 
/**
* @ORMEntity
* @ORMTable(name="fos_user")
*/
class User extends BaseUser
{
   /**
    * @ORMId
    * @ORMColumn(type="integer")
    * @ORMGeneratedValue(strategy="AUTO")
    */
   protected $id;
 
   public function __construct()
   {
       parent::__construct();
   }
}
